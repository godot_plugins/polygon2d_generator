tool
extends EditorPlugin

var dock

func _enter_tree():
	dock = load("res://addons/polygon2d_generator/generator.tscn").instance()
	dock.editor = get_editor_interface()
	add_control_to_bottom_panel(dock, 'poly')


func _ready():
	pass

func _exit_tree():
	remove_control_from_bottom_panel(dock)
	dock.free()

